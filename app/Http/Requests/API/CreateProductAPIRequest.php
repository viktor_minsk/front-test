<?php

namespace App\Http\Requests\API;

use App\Models\Product;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use InfyOm\Generator\Request\APIRequest;
use InfyOm\Generator\Utils\ResponseUtil;

class CreateProductAPIRequest extends APIRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


     protected function failedValidation(Validator $validator)
     {

         throw new HttpResponseException(

             response()->json(
                 ['success'=>false, 'message'=>'Invalid data supplied', 'errors'=>(array) $validator->errors()->toArray()]
                 , 422));
     }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return Product::$rules;
    }
}
