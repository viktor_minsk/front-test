<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       
            Schema::defaultStringLength(191);
            Validator::extend('pcode', function ($attribute, $value, $parameters, $validator) {
                $a=explode('-',$value);
                if(count($a)!=2) return false;
                
                list($a,$b)=$a;
                
                if( !is_numeric($a) || !is_numeric($b)  || strlen($a)!=4 || strlen($b)!=4 ) return false;
                return true;
            });
            
            Validator::replacer('pcode', function ($message, $attribute, $rule, $parameters) {
                return 'Product code is invalid';
            });
        //
    }
}
