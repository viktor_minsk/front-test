<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Product
 * @package App\Models
 * @version June 13, 2019, 7:54 pm UTC
 *
 * @property string name
 * @property string code
 * @property float price
 */
class Product extends Model
{

    public $table = 'products';
    


    public $fillable = [
        'name',
        'code',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'price' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'code' => ['required', 'pcode'],
        'price' => ['required', 'numeric']
    ];

    
}
